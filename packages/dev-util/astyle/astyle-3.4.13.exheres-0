# Copyright 2009, 2010 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.bz2 ] \
    bash-completion \
    cmake \
    zsh-completion

SUMMARY="A source code indenter, formatter, and beautifier for the C, C++, C# and Java programming languages"

UPSTREAM_CHANGELOG="https://astyle.sourceforge.net/notes.html"
UPSTREAM_RELEASE_NOTES="https://astyle.sourceforge.net/news.html"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    fish-completion  [[ description = [ Install completion files for the fish shell ] ]]
"

DEPENDENCIES=""

BASH_COMPLETIONS=( "${CMAKE_SOURCE}"/sh-completion/${PN}.bash )
ZSH_COMPLETIONS=( "${CMAKE_SOURCE}"/sh-completion/${PN}.zsh )

DEFAULT_SRC_PREPARE_PATCHES=(
    -p0 "${FILES}"/${PNV}-cmake-install.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_DISABLE_FIND_PACKAGE_wxWidgets:BOOL=TRUtE
    -DBUILD_JAVA_LIBS:BOOL=FALSE
    -DBUILD_SHARED_LIBS:BOOL=FALSE
    -DBUILD_STATIC_LIBS:BOOL=FALSE
)

src_install() {
    default

    insinto /usr/share/doc/${PNVR}/html
    doins "${CMAKE_SOURCE}"/doc/*

    doman "${CMAKE_SOURCE}"/man/"${PN}".1

    bash-completion_src_install
    zsh-completion_src_install
    if option fish-completion; then
        insinto /usr/share/fish
        newins "${CMAKE_SOURCE}"/sh-completion/${PN}.fish ${PN}.fish
    fi
}

