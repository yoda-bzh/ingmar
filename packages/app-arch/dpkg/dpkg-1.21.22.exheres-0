# Copyright 2008, 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'dpkg-1.14.17.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require debian-upstream [ suffix=tar.xz ]
require zsh-completion

SUMMARY="Package maintenance system for Debian"
HOMEPAGE="https://tracker.debian.org/pkg/${PN}"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~x86"
MYOPTIONS="
    selinux
    zstd
    ( linguas: ast bs ca cs da de dz el eo es et eu fr gl hu id it ja km ko ku lt mr nb ne nl nn pa
               pl pt pt_BR ro ru sk sv th tl tr vi zh_CN zh_TW )
"

# dev-perl/IO-String dev-perl/TimeDate dev-perl/Test-Pod
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
    build+run:
        app-arch/bzip2
        app-arch/xz
        dev-lang/perl:=[>=5.28.1]
        dev-libs/libmd
        sys-libs/ncurses
        sys-libs/zlib
        zstd? ( app-arch/zstd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    --enable-dselect
    --enable-nls
    --enable-static
    --disable-coverage
    --disable-shared
    --disable-start-stop-daemon
    --with-libbz2
    --with-liblzma
    --with-libz
    --with-zshcompletionsdir=${ZSHCOMPLETIONDIR}
    # I don't think we have it at the moment
    --without-libz-ng
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'selinux libselinux'
    'zstd libzstd'
)

src_install() {
    default

    # FIXME Pass the correct arch/os for dpkg, so it installs to /usr/lib64
    keepdir /etc/dpkg/{dpkg.cfg.d,dselect.cfg.d}
    keepdir /var/lib/dpkg/{alternatives,info,parts,updates}/
    keepdir /var/lib/dpkg/methods/{disk,file,floppy,ftp,media,mnt,multicd}/
    touch "${IMAGE}"/var/lib/dpkg/{available,status}

    find "${IMAGE}"/usr/share/man -empty -type d -delete

    option zsh-completion || edo rm -r "${IMAGE}"/usr/share/zsh/
}

